# adv-cam-id

This is the code associated with the paper:
C. Chen, X. Zhao and M. C. Stamm, "Generative Adversarial Attacks Against Deep-Learning-Based Camera Model Identification," in IEEE Transactions on Information Forensics and Security, 2019.